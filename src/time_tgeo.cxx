#include <iostream>
#include <string>
#include <chrono>

#include "TApplication.h"
#include "TGeoManager.h"
#include "Math/Point3D.h"
#include "Math/Vector3D.h"

#include "intervals.h"

int main(int argc, char *argv[])
{
    std::string gdml_filename = "LHCb.gdml";
    if (argc > 1)
    {
        gdml_filename = argv[1];
    }
    else
    {
        std::cerr << "Please specify the GDML file name" << std::endl;
        return 1;
    }
    std::cout << "Loading " << gdml_filename << std::endl;
    load_gdml(gdml_filename);

    std::vector<Interval> intervals;
    int count = 0;
    int total_intervals = 0;
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    double range = 1000.0;
    double zmin = -200.0;
    double zmax = 20000.0;
    for (double x = -range; x < range; x += 1.0)
    {
        for (double y = -range; y < range; y += 1.0)
        {
            std::cout << "From (" << x << ", " << y << ", " << zmin << ") to zmax: " << zmax << std::endl;
            intervals.clear();
            XYZPoint start{x, y, zmin};
            XYZPoint end{x, y, zmax};
            get_intervals(start, end, intervals, gGeoManager);
            total_intervals += intervals.size();
            count++;
            //std::cout << count << std::endl;
        }
    }

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);

    std::cout << "Total time  :" << time_span.count() << " seconds." << std::endl;
    std::cout << "Nb intervals:" <<  total_intervals << std::endl;

return 0;
}