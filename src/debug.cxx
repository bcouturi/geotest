/*
 * Just code for debugging...
 *
 *
 */
#include <iostream>
#include <string>

#include "TApplication.h"
#include "TGeoManager.h"
#include "Math/Point3D.h"
#include "Math/Vector3D.h"

using XYZPoint = ROOT::Math::XYZPoint;

struct MatIntervalCmp
{
    Double_t start;
    Double_t end;
    Double_t radlength;
    std::string materialName;
    TGeoMaterial *material;
    std::vector<std::string> locations;

    Double_t LHCbstart;
    Double_t LHCbend;
    Double_t LHCbradlength;
    std::string LHCbmaterialName;
    std::vector<std::string> LHCblocations;
};

void loadGeometry(std::string gdml_filename)
{
    TGeoManager::Import(gdml_filename.c_str());
    gGeoManager->SetVisLevel(4);
}

void getTGeoIntersections(XYZPoint start,
                          XYZPoint end,
                          std::vector<MatIntervalCmp> &intersepts,
                          TGeoManager *m)
{
    auto v = end - start;
    auto v_norm = v.unit();

    bool verbose = true;
    Double_t snext;
    TString path;
    Double_t pt[3];
    Double_t loc[3];
    Double_t epsil = 1.E-2;
    Double_t lastrad = 0.;
    Int_t ismall = 0;
    Int_t nbound = 0;
    Double_t length = 0.;
    //Double_t safe = 0.;
    Double_t rad = 0.;
    TGeoMedium *med;
    TGeoShape *shape;
    TGeoNode *lastnode;

    m->InitTrack(start.X(), start.Y(), start.Z(), v_norm.X(), v_norm.Y(), v_norm.Z());
    if (verbose)
    {
        fprintf(stderr,
                "Track: (%15.10f,%15.10f,%15.10f,%15.10f,%15.10f,%15.10f)\n",
                start.X(), start.Y(), start.Z(), v_norm.X(), v_norm.Y(), v_norm.Z());
        path = m->GetPath();
    }
    TGeoNode *nextnode = m->GetCurrentNode();
    std::cout << "LOCATION: " << nextnode->GetName() << std::endl;
    //safe = m->Safety();
    while (nextnode)
    {
        med = 0;
        if (nextnode)
            med = nextnode->GetVolume()->GetMedium();
        else
            return;
        shape = nextnode->GetVolume()->GetShape();
        lastnode = nextnode;
        nextnode = m->FindNextBoundaryAndStep();
        snext = m->GetStep();
        if (snext < 1.e-8)
        {
            ismall++;
            if ((ismall < 3) && (lastnode != nextnode))
            {
                // First try to cross a very thin layer
                length += snext;
                nextnode = m->FindNextBoundaryAndStep();
                snext = m->GetStep();
                if (snext < 1.E-8)
                    continue;
                // We managed to cross the layer
                ismall = 0;
            }
            else
            {
                // Relocate point
                if (ismall > 3)
                {
                    fprintf(stderr, "ERROR: Small steps in: %s shape=%s\n", m->GetPath(),
                            shape->ClassName());
                    return;
                }
                memcpy(pt, m->GetCurrentPoint(), 3 * sizeof(Double_t));
                const Double_t *dir = m->GetCurrentDirection();
                for (Int_t i = 0; i < 3; i++)
                    pt[i] += epsil * dir[i];
                snext = epsil;
                length += snext;
                rad += lastrad * snext;
                m->CdTop();
                nextnode = m->FindNode(pt[0], pt[1], pt[2]);
                if (m->IsOutside())
                    return;
                TGeoMatrix *mat = m->GetCurrentMatrix();
                mat->MasterToLocal(pt, loc);
                if (!m->GetCurrentVolume()->Contains(loc))
                {
                    //            fprintf(stderr,"Woops - out\n");
                    m->CdUp();
                    nextnode = m->GetCurrentNode();
                }
                continue;
            }
        }
        else
        {
            ismall = 0;
        }
        nbound++;
        length += snext;
        if (med)
        {
            Double_t radlen = med->GetMaterial()->GetRadLen();
            if (radlen > 1.e-5 && radlen < 1.e10)
            {
                lastrad = med->GetMaterial()->GetDensity() / radlen;
                rad += lastrad * snext;
            }
            else
            {
                lastrad = 0.;
            }
            if (verbose)
            {
                fprintf(stderr, " STEP #%d: %s\n", nbound, path.Data());
                fprintf(stderr, "    step=%g  length=%g  rad=%g %s\n", snext, length,
                        med->GetMaterial()->GetDensity() * snext /
                            med->GetMaterial()->GetRadLen(),
                        med->GetName());
                //fprintf(stderr, "TGeoMedium: %s\n", med->GetName());

                path = m->GetPath();
            }

            // Now filling up the data
            MatIntervalCmp cur;
            cur.start = (length - snext) / v.R();
            cur.end = (length) / v.R();
            cur.radlength = med->GetMaterial()->GetDensity() * snext /
                            med->GetMaterial()->GetRadLen();
            cur.material = med->GetMaterial();
            cur.materialName = med->GetName();
            cur.locations.push_back(m->GetPath());

            // Checking whether we need to agregate the entries in the vector
            if (intersepts.size() > 0)
            {
                MatIntervalCmp &prev = intersepts.back();
                if (prev.materialName == cur.materialName)
                {
                    prev.end = cur.end;
                    prev.radlength += cur.radlength;
                    prev.locations.push_back(cur.locations.front());
                }
                else
                {
                    // Different material, no need to agregate
                    intersepts.push_back(cur);
                }
            }
            else
            {
                // first entry in vector anyway
                intersepts.push_back(cur);
            }
        }
    }
}

void debug_intersect(XYZPoint start,
                     XYZPoint end)
{

    TGeoManager *m = gGeoManager;

    // Setting the initial point
    auto v = end - start;
    double total_dist = v.Mag2();
    auto v_norm = v.unit(); // We need this to be a unit vector for use in the TGeoNavigator

    m->InitTrack(start.X(), start.Y(), start.Z(), v_norm.X(), v_norm.Y(), v_norm.Z());

    // XXX Need to check whether we're outside the geometry with gGeoManager->IsOutside()

    TGeoNode *nextnode = m->GetCurrentNode();
    TGeoNode *prevnode = nullptr;
    TGeoNode *prevprevnode = nullptr;
    double cur_dist = 0;
    while (cur_dist < total_dist && nextnode != nullptr)
    {
        std::cout << "======> Cur dist:" << cur_dist << std::endl;
        TGeoMedium *med = nextnode->GetVolume()->GetMedium();
        TGeoShape *shape = nextnode->GetVolume()->GetShape();
        const Double_t *glob_pt = gGeoManager->GetCurrentPoint();
        fprintf(stderr,
                "Pos: (%.3f,%.3f,%.3f) %d %s %s %s\n",
                glob_pt[0], glob_pt[1], glob_pt[2], gGeoManager->GetCurrentNodeId(),
                m->GetPath(), med->GetMaterial()->GetName(), shape->GetName());
        prevprevnode = prevnode;
        prevnode = nextnode;
        nextnode = m->FindNextBoundaryAndStep();
        double snext = gGeoManager->GetStep();
        cur_dist += snext;
        // if (nextnode == prevprevnode)
        // {
        //     std::cout << "We have a loop " << nextnode->GetName() << " / " << prevnode->GetName()
        //               << " " << gGeoManager->GetPath() << std::endl;
        //     nextnode->GetMotherVolume()->CheckOverlaps();
        //     gGeoManager->PrintOverlaps();
        // }
        std::cout << "NODES: " << prevprevnode << " / " << prevnode << " / " << nextnode << std::endl;

        Double_t safety = gGeoManager->GetSafeDistance();
        fprintf(stderr, "Step/Safety: (%.3f,%.3f) \n", snext, safety);
    }
}

int main(int argc, char *argv[])
{
    //TApplication theApp("App",&argc, argv);
    std::string gdml_filename = "LHCb.gdml";
    if (argc > 1)
    {
        gdml_filename = argv[1];
    }
    else
    {
        std::cerr << "Please specify the GDML file name" << std::endl;
        return 1;
    }
    std::cout << "Loading " << gdml_filename << std::endl;
    loadGeometry(gdml_filename);

    std::vector<MatIntervalCmp> intersepts;
    XYZPoint start{-28, 19, -200};
    XYZPoint end{-28, 19, 20000};
    debug_intersect(start, end);

    for (const auto &mic : intersepts)
    {
        std::cout << mic.locations[0] << " " << mic.start << " -> " << mic.end << std::endl;
    }
    //theApp.Run();
    return 0;
}