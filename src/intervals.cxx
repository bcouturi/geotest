#include <iostream>
#include <string>

#include "TGeoManager.h"
#include "TMath.h"
#include "Math/Point3D.h"
#include "Math/Vector3D.h"

#include "intervals.h"

void load_gdml(std::string gdml_filename)
{
    TGeoManager::Import(gdml_filename.c_str());
    gGeoManager->SetVisLevel(4);
}

void get_intervals(XYZPoint start,
                   XYZPoint end,
                   std::vector<Interval> &intervals,
                   TGeoManager *m)
{

    // Setting the initial point
    auto v = end - start;
    auto v_norm = v.unit(); // We need this to be a unit vector for use in the TGeoNavigator

    m->InitTrack(start.X(), start.Y(), start.Z(), v_norm.X(), v_norm.Y(), v_norm.Z());
    // XXX Need to check whether we're outside the geometry with gGeoManager->IsOutside()

    TGeoNode *nextnode = m->GetCurrentNode();
    bool continue_scan = true;
    Int_t nstep = 0;
    Double_t total_step_len = TMath::Sqrt(v.Mag2());
    Double_t current_step_len = 0.;
    while (continue_scan)
    {
        TGeoMedium *med = nextnode->GetVolume()->GetMedium();
        TGeoShape *shape = nextnode->GetVolume()->GetShape();
        const Double_t *glob_pt = gGeoManager->GetCurrentPoint();
        nextnode = m->FindNextBoundaryAndStep(total_step_len - current_step_len);
        Double_t snext = gGeoManager->GetStep();

        if (snext < 0)
        {
            std::cout << "We have a negative step, next node is: " << nextnode->GetName() << " / " << gGeoManager->GetPath() << std::endl;
            continue_scan = false;
            break;
        }

        // Now filling up the data
        Interval cur;
        cur.start = current_step_len;
        cur.end = current_step_len + snext;
        cur.radlength = med->GetMaterial()->GetDensity() * snext /
                        med->GetMaterial()->GetRadLen();
        cur.material = med->GetMaterial();
        cur.materialName = med->GetName();
        cur.locations.push_back(m->GetPath());

        // Checking whether we need to agregate the entries in the vector
        if (intervals.size() > 0)
        {
            Interval &prev = intervals.back();
            if (prev.materialName == cur.materialName)
            {
                prev.end = cur.end;
                prev.radlength += cur.radlength;
                prev.locations.push_back(cur.locations.front());
            }
            else
            {
                // Different material, no need to agregate
                intervals.push_back(cur);
            }
        }
        else
        {
            // first entry in vector anyway
            intervals.push_back(cur);
        }

        current_step_len += snext;
        // Check the stop condition
        if (!nextnode || (total_step_len - current_step_len < 1.e-8))
        {
            // fprintf(stderr,
            //         "#%d: Cumulated length: %.3f  Last position: (%.3f,%.3f,%.3f) %d %s\n",
            //         nstep++, current_step_len, glob_pt[0], glob_pt[1], glob_pt[2], gGeoManager->GetCurrentNodeId(),
            //         m->GetPath());
            break;
        }
    }
}
