#pragma once

#include <vector>

#include <TGeoManager.h>

using XYZPoint = ROOT::Math::XYZPoint;

struct Interval
{
    Double_t start;
    Double_t end;
    Double_t radlength;
    TGeoMaterial *material;
    std::string materialName;
    std::vector<std::string> locations;
};

void load_gdml(std::string gdml_filename);

void get_intervals(XYZPoint start,
                   XYZPoint end,
                   std::vector<Interval> &intervals,
                   TGeoManager *m);